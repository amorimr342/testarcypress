# Iniciando o git
Instruções de linha de comando
Você também pode fazer upload de arquivos existentes de seu computador usando as instruções abaixo.
criando pasta 
* mkdir NOME_PASTA
Entrando na pasta criada
* cd NOME_PASTA
* git init
Configuração  do Git
* git config user.name {{"SEU NOME"}}
* git config user.email {{"SEU@EMAIL.COM"}}
* git remote add origin git@gitlab.com:{{SEU_GIT_LAB}}/{{NOME_SEU_ROPOSITORIO}}.git
* echo "/node_modules" >> ".gitignore"
* echo "# Meu projeto" >> "README.md"

# Instalando cypress

Installing Cypress via yarn:
* yarn add cypress --dev

inicializando cypress

* yarn run cypress open

Apagar pasta exemplo do Cypress

Criando primerio teste

echo "/// <reference types='cypress' />
describe('Primeiro Teste', () => {
    it('Estrutura do teste cypress', () => {
        cy.visit('https://docs.cypress.io/');
    });
});" >> "cypress/integration/primeiroTeste.spec.js"

# Liberadno permissão para subir codigo, apenas 1 vez
Logado no git lab, copiar e colar no navegador
https://gitlab.com/-/profile/keys

instalar xclip para copiar SSH KEY gitlab
* sudo apt install xclip

copiar SSH KEY gitlab
* xclip -sel clip < ~/.ssh/id_ed25519.pub

Voltar ao navegador e colar o texto que o xclip copiou do terminal

# Enviando Alterações da branch local para o repositório

Conferindo quais arquivos estão disponiveis para subir no GITLAB
* git status

Confirmando a branch atual
* git branch

Adicionando os arquivos alterados para subir no Git
* git add.

Commitando arquivos alterados e adicionado para subir no gitLab
* git commit -m "commit inicial"

Publicando as alterações no serviço 
* git push -u origin master
