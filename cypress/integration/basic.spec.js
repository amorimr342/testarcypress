/// <reference types = "cypress" />

describe("Cypress basics", () => {
    it.only('shoould visit page and assert title', () => {
        cy.visit("https://wcaquino.me/cypress/componentes.html")

        ///const title = cy.title
        ///console.log(title)

        cy.title().should("be.equal", "Campo de Treinamento");
        cy.title().should("contain", "Campo")

        cy.title()
            .should("be.equal", "Campo de Treinamento")
            .and("contain", "Campo")

        let syncTitle

        cy.title().then(title => {
            console.log(title)

            cy.get('#formNome').type(title)

            syncTitle = title
        })

        cy.get('[data-cy=dataSobrenome]').then($el => {
            $el.val(syncTitle)
        })

        cy.get('#elementosForm\\:sugestoes').then($el => {
            cy.wrap($el).type(syncTitle)
        })
            //cy.title().debug()
            //cy.title().then(title => {
                //console.log(title)

                cy.title().should(title => {
                    console.log(title)
            })
            //TODO imprimir o log no console
            //TODO escrever o log em um campo de texto
    });

    it('Shoudl find and interact witl an elemet', () => {
        cy.visit("https://wcaquino.me/cypress/componentes.html")

        //cy.get("nao existe")

        cy.get('#buttonSimple')
        .click()
        .should('have.value', 'Obrigado!')


    });
})
